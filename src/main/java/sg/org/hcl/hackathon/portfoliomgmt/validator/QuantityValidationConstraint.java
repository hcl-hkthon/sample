package sg.org.hcl.hackathon.portfoliomgmt.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = QuantityValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface QuantityValidationConstraint {
    String message() default "Invalid Quantity.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
