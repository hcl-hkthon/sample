package sg.org.hcl.hackathon.portfoliomgmt.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BalanceResponse extends Response{

    @JsonInclude
    private Double balance;
}
