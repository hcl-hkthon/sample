package sg.org.hcl.hackathon.portfoliomgmt.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = BalanceValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface BalanceValidationConstraint {
    String message() default "Not found enough balance.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
