package sg.org.hcl.hackathon.portfoliomgmt.exception;

import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
public class ValidationErrorResponse {
    private String errorCode;
    private Timestamp timestamp;
    private List<String> validationErrors;
}
