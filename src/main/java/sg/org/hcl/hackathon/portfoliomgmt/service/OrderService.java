package sg.org.hcl.hackathon.portfoliomgmt.service;

import sg.org.hcl.hackathon.portfoliomgmt.request.OrderRequest;
import sg.org.hcl.hackathon.portfoliomgmt.response.OrderResponse;

public interface OrderService {
    OrderResponse create(OrderRequest orderRequest);
}
