package sg.org.hcl.hackathon.portfoliomgmt.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class PmsExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
              HttpHeaders headers, HttpStatus status, WebRequest request) {
        final List<String> errors = new ArrayList<>(0);
        for (FieldError error: ex.getBindingResult().getFieldErrors()){
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error: ex.getBindingResult().getGlobalErrors()){
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        final ValidationErrorResponse validationErrorResponse = ValidationErrorResponse.builder()
                .errorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()))
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .validationErrors(errors)
                .build();
        return this.handleExceptionInternal(ex, validationErrorResponse, headers, status, request);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<ValidationErrorResponse> handleConstraintViolation(ConstraintViolationException ex, WebRequest request){
        List<String> errors = new ArrayList<>(0);
        for(ConstraintViolation<?> violation: ex.getConstraintViolations()){
            errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": " + violation.getMessage());
        }

        final ValidationErrorResponse validationErrorResponse = ValidationErrorResponse.builder()
                .errorCode(Integer.toString(HttpStatus.BAD_REQUEST.value()))
                .timestamp(Timestamp.valueOf(LocalDateTime.now()))
                .validationErrors(errors)
                .build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationErrorResponse);
    }

    @ExceptionHandler(value = PmsException.class)
    protected ResponseEntity<ErrorResponse> handlePolicyGenericException(PmsException ex){
        logger.error(ex.toString());

        final ErrorResponse errorResponse = ErrorResponse.builder()
                .errorCode(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()))
                .errorMessage(ex.getErrorMessage())
                .timestamp(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }
}
