package sg.org.hcl.hackathon.portfoliomgmt.validator;

import org.springframework.beans.factory.annotation.Autowired;
import sg.org.hcl.hackathon.portfoliomgmt.entity.PortfolioDetails;
import sg.org.hcl.hackathon.portfoliomgmt.repo.PortfolioRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BalanceValidator implements ConstraintValidator<BalanceValidationConstraint, Double>{

    @Autowired
    private PortfolioRepository portfolioRepository;

    @Override
    public boolean isValid(Double balance, ConstraintValidatorContext context) {
        final PortfolioDetails portfolioDetails = portfolioRepository.findById("1").orElse(null);
        return portfolioDetails.getRunningBalance() > balance;
    }
}
