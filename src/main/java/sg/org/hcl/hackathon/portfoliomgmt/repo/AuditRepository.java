package sg.org.hcl.hackathon.portfoliomgmt.repo;

import org.springframework.data.repository.CrudRepository;
import sg.org.hcl.hackathon.portfoliomgmt.entity.Audit;

public interface AuditRepository extends CrudRepository<Audit, String> {
}
