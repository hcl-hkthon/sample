package sg.org.hcl.hackathon.portfoliomgmt.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class OrderResponse extends Response {

    @JsonInclude
    private String orderId;

    @JsonInclude
    private String status;

}
