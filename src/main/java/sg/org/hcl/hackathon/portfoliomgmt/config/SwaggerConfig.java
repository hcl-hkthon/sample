package sg.org.hcl.hackathon.portfoliomgmt.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    @Value("${app.swagger.api.title}")
    private String apiTitle;

    @Bean
    public OpenAPI customOpenApi(){
        return new OpenAPI().info(
                new Info().title(apiTitle)
        );
    }
}
