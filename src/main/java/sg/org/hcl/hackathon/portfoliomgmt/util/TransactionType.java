package sg.org.hcl.hackathon.portfoliomgmt.util;

public enum TransactionType {
    BUY,
    SELL
}
