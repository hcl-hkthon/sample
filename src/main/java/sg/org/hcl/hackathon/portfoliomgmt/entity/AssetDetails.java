package sg.org.hcl.hackathon.portfoliomgmt.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@SuperBuilder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
@Table(name = "ASSET_DETAILS")
public class AssetDetails extends BaseEntity{

    @Column(name = "NAME")
    private String assetName;
}
