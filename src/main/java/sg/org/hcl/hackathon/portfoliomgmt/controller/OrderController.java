package sg.org.hcl.hackathon.portfoliomgmt.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sg.org.hcl.hackathon.portfoliomgmt.request.OrderRequest;
import sg.org.hcl.hackathon.portfoliomgmt.response.BalanceResponse;
import sg.org.hcl.hackathon.portfoliomgmt.response.OrderResponse;
import sg.org.hcl.hackathon.portfoliomgmt.service.OrderService;
import sg.org.hcl.hackathon.portfoliomgmt.service.PortfolioService;
import sg.org.hcl.hackathon.portfoliomgmt.validator.DBConnectionValidator;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.sql.SQLException;

@Validated
@RestController
@RequestMapping(path = "/pms")
@Slf4j
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private PortfolioService portfolioService;

    @Autowired
    private DBConnectionValidator dbConnectionValidator;

    @PostMapping(path = "/order")
    public ResponseEntity<OrderResponse> originateOrder(@RequestBody @NotNull @Valid OrderRequest order){
        final OrderResponse orderResponse = orderService.create(order);
        return new ResponseEntity<OrderResponse>(orderResponse, HttpStatus.OK);
    }

    @GetMapping(path = "/myBalance/{portfolioId}")
    public ResponseEntity<BalanceResponse> displayBalance(@PathVariable(required = true) final String portfolioId){
        final BalanceResponse balanceResponse = portfolioService.getBalance(portfolioId);
        return new ResponseEntity<BalanceResponse>(balanceResponse, HttpStatus.OK);
    }

    @Operation(hidden = true)
    @GetMapping(path = "/health")
    public String healthCheck()throws SQLException {
        return dbConnectionValidator.checkValidity();
    }
}
