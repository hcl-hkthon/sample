package sg.org.hcl.hackathon.portfoliomgmt.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = SecurityNameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface SecurityNameValidationConstraint {
    String message() default "Fund Name doesn't exist.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
