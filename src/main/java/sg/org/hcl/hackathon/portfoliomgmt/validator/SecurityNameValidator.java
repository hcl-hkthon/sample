package sg.org.hcl.hackathon.portfoliomgmt.validator;

import org.springframework.beans.factory.annotation.Autowired;
import sg.org.hcl.hackathon.portfoliomgmt.repo.AssetRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class SecurityNameValidator implements ConstraintValidator<SecurityNameValidationConstraint, String> {

    @Autowired
    private AssetRepository assetRepository;

    @Override
    public boolean isValid(String securityName, ConstraintValidatorContext context) {
        return Objects.nonNull(assetRepository.findByAssetName(securityName));
    }
}
