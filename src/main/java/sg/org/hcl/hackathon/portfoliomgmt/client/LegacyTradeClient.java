package sg.org.hcl.hackathon.portfoliomgmt.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class LegacyTradeClient {

    @Value("${app.legacy.app.base.url}")
    private String legacyAppBaseUrl;

    private WebClient webClient = WebClient.create(legacyAppBaseUrl);

    public void pushOrderToLegacyApp(){
        Mono<String> orderProcess = webClient.get()
                .uri("/trade/book")
                .retrieve()
                .bodyToMono(String.class);

        orderProcess.subscribe(log::info);
    }
}
