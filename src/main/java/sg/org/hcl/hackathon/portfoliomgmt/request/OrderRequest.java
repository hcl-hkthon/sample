package sg.org.hcl.hackathon.portfoliomgmt.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import sg.org.hcl.hackathon.portfoliomgmt.validator.BalanceValidationConstraint;
import sg.org.hcl.hackathon.portfoliomgmt.validator.QuantityValidationConstraint;
import sg.org.hcl.hackathon.portfoliomgmt.validator.SecurityNameValidationConstraint;
import sg.org.hcl.hackathon.portfoliomgmt.validator.TransactionTypeValidationConstraint;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class OrderRequest implements Serializable {
    private static final long serialVersionUID = -5070797932611025445L;

    @NotBlank
    @JsonInclude
    @SecurityNameValidationConstraint
    private String fundName;

    @NotBlank
    @JsonInclude
    @TransactionTypeValidationConstraint
    private String transactionType;

    @NotNull
    @JsonInclude
    @QuantityValidationConstraint
    private Integer quantity;

    @NotNull
    @JsonInclude
    @BalanceValidationConstraint
    private Double orderValue;

    @Override
    public String toString(){
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("fundName", fundName)
                .append("transactionType", transactionType)
                .append("quantity", quantity)
                .append("orderValue", orderValue)
                .toString();
    }
}
