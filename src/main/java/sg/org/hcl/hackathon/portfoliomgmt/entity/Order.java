package sg.org.hcl.hackathon.portfoliomgmt.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;
import sg.org.hcl.hackathon.portfoliomgmt.util.OrderStatus;
import sg.org.hcl.hackathon.portfoliomgmt.util.TransactionType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@SuperBuilder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
@Table(name = "ORDER_DETAIL")
public class Order extends BaseEntity {

    @Column(name = "TRANSACTION_TYPE")
    private TransactionType transactionType;

    @Column(name = "TRANSACTION_DATE")
    private LocalDateTime transactionDate;

    @Column(name = "STATUS")
    private OrderStatus status;

    @Column(name = "QUANTITY")
    private Integer quantity;

    @Column(name = "BOOKED_PRICE")
    private Double bookedPrice;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "portfolio_id")
    private PortfolioDetails portFolio;
}
