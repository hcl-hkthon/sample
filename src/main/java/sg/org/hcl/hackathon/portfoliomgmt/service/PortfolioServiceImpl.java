package sg.org.hcl.hackathon.portfoliomgmt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sg.org.hcl.hackathon.portfoliomgmt.entity.PortfolioDetails;
import sg.org.hcl.hackathon.portfoliomgmt.repo.PortfolioRepository;
import sg.org.hcl.hackathon.portfoliomgmt.response.BalanceResponse;

import java.util.Optional;

@Service
public class PortfolioServiceImpl implements PortfolioService{

    @Autowired
    private PortfolioRepository portfolioRepository;

    @Override
    public BalanceResponse getBalance(String portfolioId) {
        final PortfolioDetails portfolioDetails = portfolioRepository.findById(portfolioId).orElse(null);
        final Double balance = Optional.ofNullable(portfolioDetails).map(PortfolioDetails::getRunningBalance).orElse(0.0d);
        return BalanceResponse.builder()
                .balance(balance)
                .build();
    }
}
