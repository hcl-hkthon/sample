package sg.org.hcl.hackathon.portfoliomgmt.util;

public enum OrderStatus {
    EXECUTED,
    FAILED,
    SUBMITTED,
    CANCELLED,
    COMPLETED;
}
