package sg.org.hcl.hackathon.portfoliomgmt.validator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;

@Component
@Slf4j
public class DBConnectionValidator {

    @Autowired
    private DataSource dataSource;

    public String checkValidity()throws SQLException {
        final boolean validConnection = dataSource.getConnection().isValid(10);
        log.info("DB Connection validation is: {}", validConnection);
        return validConnection ? "Application OK" : "Application Not OK";
    }
}
