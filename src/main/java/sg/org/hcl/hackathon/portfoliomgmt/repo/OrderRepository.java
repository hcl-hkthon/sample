package sg.org.hcl.hackathon.portfoliomgmt.repo;

import org.springframework.data.repository.CrudRepository;
import sg.org.hcl.hackathon.portfoliomgmt.entity.Order;

public interface OrderRepository extends CrudRepository<Order, String> {
}
