package sg.org.hcl.hackathon.portfoliomgmt.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class PaymentRequest implements Serializable {
    private static final long serialVersionUID = -2153727740633085397L;

    private String paymentType;
    private String orderId;
    private Double amount;
}
