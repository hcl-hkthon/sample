package sg.org.hcl.hackathon.portfoliomgmt.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QuantityValidator implements ConstraintValidator<QuantityValidationConstraint, Integer>{
    @Override
    public boolean isValid(Integer quantity, ConstraintValidatorContext context) {
        return quantity > 0;
    }
}
