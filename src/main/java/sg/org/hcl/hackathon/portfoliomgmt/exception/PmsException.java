package sg.org.hcl.hackathon.portfoliomgmt.exception;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Data
public class PmsException extends RuntimeException{

    private final String reference;
    private final String shortMessage;
    private final String errorMessage;

    public PmsException(String reference, String shortMessage, String errorMessage){
        this.reference = reference;
        this.shortMessage = shortMessage;
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString(){
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("reference", reference)
                .append("shortMessage", shortMessage)
                .append("errorMessage", errorMessage)
                .toString();
    }
}
