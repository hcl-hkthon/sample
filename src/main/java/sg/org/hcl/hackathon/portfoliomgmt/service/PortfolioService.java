package sg.org.hcl.hackathon.portfoliomgmt.service;

import sg.org.hcl.hackathon.portfoliomgmt.response.BalanceResponse;

public interface PortfolioService {
    BalanceResponse getBalance(String portfolioId);
}
