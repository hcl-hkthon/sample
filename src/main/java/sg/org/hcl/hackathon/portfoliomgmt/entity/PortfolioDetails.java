package sg.org.hcl.hackathon.portfoliomgmt.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@SuperBuilder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
@Table(name = "PORTFOLIO_DETAILS")
public class PortfolioDetails extends BaseEntity{

    @Column(name = "NAME")
    private String name;

    @Column(name = "RUNNING_BALANCE")
    private Double runningBalance;
}
