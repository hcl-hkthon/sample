package sg.org.hcl.hackathon.portfoliomgmt.util;

import org.springframework.stereotype.Component;
import sg.org.hcl.hackathon.portfoliomgmt.request.PaymentRequest;

import java.util.LinkedList;
import java.util.Queue;

/*
Instead we can replace this with kafka topic or enterprise level queue implementation
* **/
@Component
public class MockPaymentDoor {
    private Queue<PaymentRequest> paymentQueue = new LinkedList<>();

    public void sendPayment(final PaymentRequest paymentRequest){
        paymentQueue.add(paymentRequest);
    }
}
