package sg.org.hcl.hackathon.portfoliomgmt.exception;

import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Builder
@Data
public class ErrorResponse {
    private String errorCode;
    private String errorMessage;
    private Timestamp timestamp;
}
