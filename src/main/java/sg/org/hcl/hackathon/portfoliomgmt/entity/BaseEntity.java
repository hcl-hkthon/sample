package sg.org.hcl.hackathon.portfoliomgmt.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@SuperBuilder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@MappedSuperclass
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 9158062317116775502L;
//
//    @Version
//    private Integer version;

    @Id
    @Column(name = "ID")
    private String id;

}
