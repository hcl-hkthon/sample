package sg.org.hcl.hackathon.portfoliomgmt.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import sg.org.hcl.hackathon.portfoliomgmt.entity.AssetDetails;

public interface AssetRepository extends CrudRepository<AssetDetails, String> {
    @Query("SELECT a FROM AssetDetails a WHERE a.assetName=:assetName")
    public AssetDetails findByAssetName(String assetName);
}
