package sg.org.hcl.hackathon.portfoliomgmt.validator;

import sg.org.hcl.hackathon.portfoliomgmt.util.TransactionType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class TransactionTypeValidator implements ConstraintValidator<TransactionTypeValidationConstraint, String>{
    @Override
    public boolean isValid(String transactionType, ConstraintValidatorContext context) {
        return Arrays.stream(TransactionType.values()).filter(txnType -> txnType.name().equalsIgnoreCase(transactionType)).findFirst().isPresent();
    }
}
