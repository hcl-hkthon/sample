package sg.org.hcl.hackathon.portfoliomgmt.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/trade")
@Slf4j
public class MockAppController {

    @GetMapping(path = "/book")
    public ResponseEntity<String> bookTrade(){
        try {
            Thread.sleep(1);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
    }
}
