package sg.org.hcl.hackathon.portfoliomgmt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sg.org.hcl.hackathon.portfoliomgmt.client.LegacyTradeClient;
import sg.org.hcl.hackathon.portfoliomgmt.entity.Audit;
import sg.org.hcl.hackathon.portfoliomgmt.entity.Order;
import sg.org.hcl.hackathon.portfoliomgmt.entity.PortfolioDetails;
import sg.org.hcl.hackathon.portfoliomgmt.exception.PmsException;
import sg.org.hcl.hackathon.portfoliomgmt.repo.AuditRepository;
import sg.org.hcl.hackathon.portfoliomgmt.repo.OrderRepository;
import sg.org.hcl.hackathon.portfoliomgmt.repo.PortfolioRepository;
import sg.org.hcl.hackathon.portfoliomgmt.request.OrderRequest;
import sg.org.hcl.hackathon.portfoliomgmt.request.PaymentRequest;
import sg.org.hcl.hackathon.portfoliomgmt.response.OrderResponse;
import sg.org.hcl.hackathon.portfoliomgmt.util.MockPaymentDoor;
import sg.org.hcl.hackathon.portfoliomgmt.util.OrderStatus;
import sg.org.hcl.hackathon.portfoliomgmt.util.TransactionType;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PortfolioRepository portfolioRepository;

    @Autowired
    private AuditRepository auditRepository;

    @Autowired
    private LegacyTradeClient legacyTradeClient;

    @Autowired
    private MockPaymentDoor paymentDoor;

    @Override
    public OrderResponse create(OrderRequest orderRequest) {
        final Order order = requestToOrder(orderRequest);
        try {
            legacyTradeClient.pushOrderToLegacyApp();
        }catch (RuntimeException e){
            throw new PmsException(orderRequest.toString(), "An error in Legacy Trading system", e.getMessage());
        }
        //process payment here in the payment queue and failed payments would be in Dqueu until get processed by the job
        paymentDoor.sendPayment(new PaymentRequest());
        final Order createdOrder = orderRepository.save(order);
        final Audit audit = createAuditDetails(createdOrder);
//        portfolioRepository.save(order.getPortFolio());
        auditRepository.save(audit);
        return OrderResponse.builder()
                .orderId(createdOrder.getId())
                .status(createdOrder.getStatus().name())
                .build();
    }

    /*
    Assuming portfolio details are static
    **/
    private Order requestToOrder(final OrderRequest orderRequest){

        final PortfolioDetails portfolioDetails = portfolioRepository.findById("1").orElse(null);
        if(portfolioDetails.getRunningBalance() < orderRequest.getOrderValue()){
            throw new PmsException(orderRequest.toString(), "Running balance is lower than the requested order value", Double.toString(portfolioDetails.getRunningBalance() - orderRequest.getOrderValue()));
        }
        portfolioDetails.setRunningBalance(portfolioDetails.getRunningBalance() - orderRequest.getOrderValue());

        return Order.builder()
                .id(UUID.randomUUID().toString())
                .transactionType(Arrays.stream(TransactionType.values()).filter(tt -> tt.name().equalsIgnoreCase(orderRequest.getTransactionType())).findFirst().orElse(null))
                .transactionDate(LocalDateTime.now())
                .status(OrderStatus.SUBMITTED)
                .quantity(orderRequest.getQuantity())
                .bookedPrice(orderRequest.getOrderValue())
                .portFolio(portfolioDetails)
                .build();
    }

    private Audit createAuditDetails(final Order order){
        return Audit.builder()
                .id(UUID.randomUUID().toString())
                .createdBy(Optional.ofNullable(order.getPortFolio()).map(PortfolioDetails::getId).orElse(null))
                .createdTime(LocalDateTime.now())
                .build();
    }
}
