package sg.org.hcl.hackathon.portfoliomgmt.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = TransactionTypeValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface TransactionTypeValidationConstraint {
    String message() default "Invalid Transaction Type.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
