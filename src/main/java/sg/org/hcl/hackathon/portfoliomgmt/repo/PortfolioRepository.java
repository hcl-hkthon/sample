package sg.org.hcl.hackathon.portfoliomgmt.repo;

import org.springframework.data.repository.CrudRepository;
import sg.org.hcl.hackathon.portfoliomgmt.entity.PortfolioDetails;

public interface PortfolioRepository extends CrudRepository<PortfolioDetails, String> {
}
