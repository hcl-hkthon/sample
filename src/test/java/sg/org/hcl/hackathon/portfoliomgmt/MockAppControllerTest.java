package sg.org.hcl.hackathon.portfoliomgmt;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.Assert;

@SpringBootTest
@AutoConfigureMockMvc
public class MockAppControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void testBookTrade()throws Exception{
        final MvcResult webResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/trade/book"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("SUCCESS")))
                .andReturn();
        final String message = webResult.getResponse().getContentAsString();
        Assert.isTrue(message.contains("SUCCESS"), "Legacy App is fine");
    }
}
