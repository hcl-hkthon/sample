package sg.org.hcl.hackathon.portfoliomgmt;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.Assert;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@SpringBootTest
@AutoConfigureMockMvc
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testDisplayBalance()throws Exception{
        final MvcResult webResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/pms/myBalance/{portfolioId}", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("9990.0")))
                .andReturn();
        final String message = webResult.getResponse().getContentAsString();
        Assert.isTrue(message.contains("9990.0"), "Display Balance is fine");
    }

    @Test
    void testOriginateOrder()throws Exception{
        final String orderRequestJson = readJsonFile("/orderRequest.json");
        final MvcResult webResult = mockMvc.perform(
                MockMvcRequestBuilders.post("/pms/order/")
                        .content(orderRequestJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("orderId")))
                .andReturn();
        final String message = webResult.getResponse().getContentAsString();
        Assert.isTrue(message.contains("orderId"), "Create order is fine");
    }


    public static String readJsonFile(final String fileName) throws IOException {
        final InputStream inputStream = OrderControllerTest.class.getResourceAsStream(fileName);
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader
                (inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        }
        return textBuilder.toString();
    }
}
