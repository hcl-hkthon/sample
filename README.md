# HCL Hackathon 26/03/2022 #

This is a sample project to cover the functionality, code readability, simplicity and quality.

### What is this sample hacking? ###

* junit
* validation
* exception handling
* logging
* code coverage
* API documentation
* Auto healing readiness of application